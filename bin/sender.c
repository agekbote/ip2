#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <signal.h>

#include "common.h"
#include "sender.h"

/*
 Sends buffer (MSS sized) to each receiver for whom an ACK has not been
 received yet.

 This functions is called at start (the ACK array is all 0's) and during
 retransmit when the packet is sent only to those receivers for whom an
 ACK hasn't been received    

 */
void send_packet() {
    int i = 0, numbytes;
    struct message_t msg;
    struct message_t *hdr ;

    for(i=0; i<num_servers; i++) {

        if(ack_array[i]) {
            debug_printf("%i Ack is 1", i);
            continue;
        }

        prepare_msg(&msg, next_seq, 0, packet_msg_type);
        debug_printf("Sending Seq 0x%x for receiver : %d\n", msg.sequence_no, i);
        memcpy((void *)send_buffer, (void *)&msg, sizeof(struct message_t)); 
        memcpy(((char *)send_buffer + sizeof(struct message_t)), buffer, wnd);

        hdr = (struct message_t *) send_buffer ;
        hdr->checksum  =  compute_checksum(send_buffer, 
                            wnd + sizeof(struct message_t)) ;

        //hexdump((void *)send_buffer, wnd + sizeof(struct message_t));
        if ((numbytes = sendto(receivers[i].fd, send_buffer, 
                            wnd + sizeof(struct message_t), 0,
                        (receivers[i].p)->ai_addr, 
                        (receivers[i].p)->ai_addrlen)) == -1) {
            perror("talker: sendto");
            exit(1);
        }
    }
}


/*
    Timer thread which is periodically fired.
    This thread sends packets (by invoking send_packet) to receivers
    from whom an ACK hasn't been received yet.
 */
void timer_decrement(void *p, void *args){

    int i = 0;
    struct timeval timeout;
    int seq = next_seq; 

    while (1) {
        if (num_acks_received == num_servers) {
            break;            
        }

        timeout.tv_sec = 0;
        timeout.tv_usec = 150 * 1000;
        select (0 ,NULL, NULL, NULL, (struct timeval *)&timeout); 

        if (seq != next_seq) {
            return;
        }

        printf("Timeout, Sequence Number = 0x%x\n", next_seq);
        send_packet();
    }
    debug_printf("Returning from timer thread\n");
}

/*
    This functionis invoked at the first retransmit for every buffer.
    The fucntion transmits the buffer to each receiver and then
    waits for ACKs from all of them.
 */
void * rdt_send(void * arg) {

    int i = 0, j = 0, numbytes, received_acks = 0;
    struct message_t ack_msg;
    fd_set fds, readfds;
    int rc, clientaddrlen;
    struct sockaddr_storage their_address;
    socklen_t addrlen  = sizeof(their_address);
    struct timespec timeout;

    for (i = 0; i < num_servers; i++) {
        ack_array[i] = 0;
    }

    FD_ZERO(&fds);
    for(i=0; i<num_servers; i++) {
        FD_SET(receivers[i].fd, &fds);
    }

    memset((void *)ack_array, 0, num_servers * sizeof(unsigned short int));

    send_packet();
    num_acks_received = 0;
    pthread_create(&timer_thread, NULL, (void *)timer_decrement, 0);
    debug_printf("Starting timer for seq# : 0x%x\n", next_seq);

    debug_printf("Waiting for ACK\n");
    while(num_acks_received < num_servers) {

        readfds = fds;
        
        timeout.tv_sec = 0;
        timeout.tv_nsec = 0;
        rc = select(FD_SETSIZE, &readfds, NULL, NULL, 
                (struct timeval *)&timeout);
        if (rc == -1) {
            perror("Select");
            break;
        }

        for (i = 0; i < FD_SETSIZE; i++) {
            if (FD_ISSET(i, &readfds)) {
                numbytes = recvfrom(i, &ack_msg, 
                        sizeof(struct message_t), 0, 
                        (struct sockaddr *)&their_address, &addrlen);
                //dump_msg(&ack_msg);
                if(ack_msg.sequence_no == next_seq) {
                    for(j = 0 ; j < num_servers; j++) {
                        if( !strcmp(
                            (((receivers[j].p)->ai_addr)->sa_data), 
                            (((struct sockaddr *)&their_address)->sa_data)) ) {

                            if(!ack_array[j]) {
                                num_acks_received++;  
                            }
                            ack_array[j] = 1;
                        }        
                    }
                }
                FD_CLR(i, &readfds);
            }
        }
    }
    next_seq++;
    debug_printf("next seq 0x%x\n", next_seq);
    pthread_cancel(timer_thread);
}

/*
    Reads MSS sized bytes into the buffer and call rdt_send to multicast
    the buffer to the receivers
 */
int fileread() {

    int read_bytes  = 0;
    int total_bytes_read = 0, bytes_remaining = 0;
    char * p;
    FILE *file = fopen(filename, "rb");

    if (!file) {
        printf("Can't open file for reading");
        return;
    }

    wnd = mss;
    fseek(file,0,SEEK_SET) ;
    while (!feof(file)){

        bytes_remaining = mss;
        total_bytes_read = 0;
        char * p = buffer;
        while(total_bytes_read < mss) {

            read_bytes = fread(p, 1, bytes_remaining, file);
#if 0
            if(read_bytes != bytes_remaining) {
                printf("Aborting now\n");
                abort();
            }
#endif
            p += read_bytes;
            bytes_remaining -= read_bytes;
            total_bytes_read += read_bytes;

            if(feof(file)) {
                wnd = total_bytes_read;
                //*p = EOF;
                packet_msg_type = EOF_FILE;
                debug_printf("\n\nLast packet, wnd : %d\n\n", total_bytes_read);
                break; 
            }

        }
        debug_printf("\n\nFirst Transmit phase \n\n");
        rdt_send(0);
    }
    fclose(file);
}

/*
    Extracts information (ip address, port name) of the receivers from the
    command line arguments and stores it in the global receiver array.
 */
int extract_info_from_args(int argc, char * argv[]) {

    int i = 0, server_index = 0, rv = 0;
    int sockfd;
    struct addrinfo hints, *servinfo, *p;

    mss = atoi(argv[argc -1]);
    strcpy(filename, argv[argc - 2]);
    num_servers = argc - 4;

    debug_printf("MSS : %d, filename :%s, serverport : %s, num_servers : %d\n", 
            mss, filename, argv[argc -3], num_servers);

    buffer = (char *)malloc(BUFFER_MULTIPLIER * mss);
    if (!buffer) {
        printf("Unable to allocate memory\n");
        abort();
    }
    memset((void *)buffer, 0, BUFFER_MULTIPLIER * mss);

    send_buffer = (char *)malloc(mss + sizeof(struct message_t));
    if (!send_buffer) {
        printf("Unable to allocate memory\n");
        abort();
    }
    memset((void *)send_buffer, 0, mss + sizeof(struct message_t));

    receivers = (struct receiver_info *)malloc(num_servers * 
            sizeof(struct receiver_info));
    if(!receivers) {
        perror("receivers : cannot allocate memory\n");
    }

    ack_array = (unsigned short int *)malloc(num_servers * 
            sizeof(unsigned short int));
    if(!ack_array) {
        printf("Unable to allocate memory\n");
        abort();
    }
    memset((void *)ack_array, 0, num_servers * sizeof(unsigned short int));

    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_DGRAM;

    for(i = 0, server_index = 1; i < (argc - 3); i++, server_index++) {

        if ((rv = getaddrinfo(argv[server_index], argv[argc - 3], &hints, 
                        &(receivers[i].servinfo))) != 0) {
            fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
            return 1;
        }

        for(p = (receivers[i].servinfo); p != NULL; p = p->ai_next) {
            if ((sockfd = socket(p->ai_family, p->ai_socktype,
                            p->ai_protocol)) == -1) {
                perror("talker: socket");
                continue;
            }
            break;
        }

        if (p == NULL) {
            fprintf(stderr, "talker: failed to bind socket\n");
            return 2;
        }
        receivers[i].fd = sockfd;
        receivers[i].p = p;
    }
}

/*
    Cleaning receiver related information at the end
 */
void free_sockets() {
    int i = 0;
    for (i = 0; i < num_servers; i++) {
        freeaddrinfo(receivers[i].servinfo);
        close(receivers[i].fd); 
    }
}

int main(int argc, char *argv[])
{
    int sockfd;
    struct addrinfo hints, *servinfo, *p;
    int rv;
    int numbytes;

    if (argc < 5) {
        fprintf(stderr,"usage: p2mpclient server-1 server-2 server-3 "
            "server-port# file-name MSS\n");
        exit(1);
    }
    packet_msg_type = DATA_MSG;

    next_seq = START_SEQNO;
    extract_info_from_args(argc, argv);
    fileread();
    free_sockets();
    return 0;
}
