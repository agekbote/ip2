#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include "common.h"
#include "receiver.h"
#define MAXBUFLEN 10000

/*
    Extract seq# from the packet   
 */
__uint32_t get_sequence_no(char *buf ) {

    char *temp ; 
    temp = buf ; 
    __uint32_t *sequence = (int *) temp ;
    //    debug_printf("Sequence Number is %d\n", *sequence) ;
    return *sequence ; 
}

/*
    Get offset to the data buffer from the packet start
 */
char * get_data(char * buf ) {
#if 0
    char *temp ; 
    temp = buf ; 

    temp = temp + sizeof(__uint32_t) +  sizeof(__uint32_t); 
    int  length = strlen(temp) ;
    char * data ; 
    //    data = (char *) malloc (sizeof(char) * (length + 1) ) ; 
    //    strcpy(data, temp) ; 
#endif
    return (char *)((char *)buf + sizeof(struct message_t)); 
}


void *get_in_addr(struct sockaddr *sa)
{
    if (sa->sa_family == AF_INET) {
        return &(((struct sockaddr_in*)sa)->sin_addr);
    }

    return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

/*
    Constructs an ACK packet (for the passed seq#)and sends the packet to the 
    sender
 */
void send_ack(int sockfd, int  sequence_no,   struct sockaddr * server_addr )
{
    struct message_t ack ;    
    int numbytes ; 
    char s[INET6_ADDRSTRLEN];
    ack.sequence_no = sequence_no ; 
    ack.checksum = 0 ;
    ack.msg_type = ACK ;

    debug_printf("Sequence Number 0x%x received. Sending Ack now\n", 
        ack.sequence_no) ;
    // hexdump((void *)&ack, sizeof(struct message_t)) ;
    numbytes = sendto(sockfd, (void *) &ack , sizeof(ack), 0, server_addr, 16);
    if ( numbytes == -1) {
        perror("talker: sendto");
        exit(1);
    }
}

/*
    Dumps the incoming buffer to the file
 */
int filewrite(FILE *fptr, char *data,int data_len){

    int rval = 0, offset = 0;
    int bytes_written = 0;
    char *temp = data ;
    temp = temp + data_len - 1  ; 

    offset = 0;
    do {
        if((bytes_written = fwrite(data + offset, 1, data_len, fptr)) < 1) {
            printf("Can't write to file\n");
            fclose(fptr);
            return EXIT_FAILURE ;
        }
        offset += bytes_written;
        data_len -= bytes_written; 
    } while (data_len) ;

    return rval ;
}

/*
    Receives packet from the network and taked various actions 

    1) Drop if checksum is incorrect
    2) Drop if packet is put of seq no range
    3) Drop is randomness falls withing set probability
    4) Send ACK if incoming seq# == expected seq#
    5) Send ACK for (expect seq# -1) if incoming seq# == (expected seq# - 1)

*/
char * rdt_receive( int sockfd, int *numbytes ) {

    struct sockaddr_storage their_addr;
    char *buf ; 
    char s[INET6_ADDRSTRLEN];
    buf = (char *) malloc(MAXBUFLEN * sizeof(char)) ;
    socklen_t addr_len;
    addr_len = sizeof their_addr;

    if ((*numbytes = recvfrom(sockfd, buf, MAXBUFLEN-1 , 0,
                    (struct sockaddr *)&their_addr, &addr_len)) == -1) {
        perror("recvfrom");
        exit(1);
    }

    struct sockaddr  *pnew ;
    pnew = (struct sockaddr *) &their_addr ;  

    if(validate_checksum (buf, *numbytes) == CHECKSUM_INVALID ) 
    { 
        printf("Incorrect Checksum. Dropping Packet\n");
        return NULL; 
    }

    struct message_t received_pkt ;
    received_pkt.sequence_no =  get_sequence_no (buf) ; 
    struct message_t * ptr = (struct message_t *)buf;
    //printf("Seq # : 0x%x\n", ptr->sequence_no);

    float random =  (float)rand() / (float)RAND_MAX ;
    //printf("Random number generated %f\n", random ) ; 
    //hexdump((void *) buf, *numbytes) ; 

    if ( random <= probablity) {

        printf("Packet loss, Sequence number = 0x%x\n", 
            received_pkt.sequence_no) ;

    } else if (expected_seq != received_pkt.sequence_no) {

        debug_printf("Incorrect Sequence Number 0x%x. Expected Seq# 0x%x\n", 
            received_pkt.sequence_no , expected_seq) ;
        if(received_pkt.sequence_no == expected_seq - 1) {
            debug_printf("Sending ACK for previous one\n");
            send_ack(sockfd, received_pkt.sequence_no , pnew ) ;
        }
    } else {
        // printf("Valid Sequence Number %d\n", received_pkt.sequence_no) ;
        expected_seq++ ;
        send_ack(sockfd, received_pkt.sequence_no , pnew ) ;
        write_to_file =  1 ;
    }

    return buf ;
}

int main(int argc, char **argv)
{
    int sockfd;
    struct addrinfo hints, *servinfo, *p;
    int rv;
    int numbytes ;
    if (argc < 4 ) {
    
        fprintf(stderr, "Usage ./p2mpserver port# filename p\n") ;
        exit(1);
    }
    
    int server_port = atoi(argv[1]) ;
    fileout = argv[2]  ;        
    probablity = atof(argv[3]) ;
    debug_printf("fileout %s\n", fileout) ;  

    srand(time(NULL));
    FILE * fptr =  fopen(fileout, "wb"); 
    if (!fptr) {
        printf("Can't open file for writing");
        return;
    }

    //getting the details of the server
    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC; // set to AF_INET to force IPv4
    hints.ai_socktype = SOCK_DGRAM;
    hints.ai_flags = AI_PASSIVE; // use my IP

    if ((rv = getaddrinfo(NULL, argv[1], &hints, &servinfo)) != 0) {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
        return 1;
    }

    // loop through all the results and bind to the first we can
    for(p = servinfo; p != NULL; p = p->ai_next) {
        if ((sockfd = socket(p->ai_family, p->ai_socktype,
                        p->ai_protocol)) == -1) {
            perror("listener: socket");
            continue;
        }

        if (bind(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
            close(sockfd);
            perror("listener: bind");
            continue;
        }

        break;
    }

    if (p == NULL) {
        fprintf(stderr, "listener: failed to bind socket\n");
        return 2;
    }

    freeaddrinfo(servinfo);
    int number = 0 ; 
    while (1) {
        char *buf = rdt_receive(sockfd, &numbytes);

        if (write_to_file == 1 && buf != NULL) {   
            write_to_file = 0;
            number++ ;
            char *data = get_data (buf) ;
            struct message_t * hdr_ptr = (struct message_t *)buf;
            numbytes = numbytes - sizeof(struct message_t) ;
            //   hexdump((void *)data, numbytes) ;
            int last_segment =  filewrite(fptr, data, numbytes); 

            if(hdr_ptr->msg_type == EOF_FILE) {
                // debug_printf("Last Segment encountered, Closing the file\n");
                fclose(fptr); 
                free(buf);
                break ; 
            }
        }
       
       if (buf!= NULL)
           free(buf) ;
    }
    close(sockfd);

    return 0;
}
