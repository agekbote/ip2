#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include "common.h"

/*          MESSAGE HANDLING FUNCTIONS      */

void dump_msg(struct message_t * ptr) {
    printf("Seq# : 0x%x\n", ptr->sequence_no);
    printf("Checksum : %d\n", ptr->checksum);
    printf("Msg_type : 0x%x\n", ptr->msg_type);
}

void prepare_msg(struct message_t * ptr, __uint32_t seq_no, 
        __uint16_t checksum, __uint16_t msg_type) {
    ptr->sequence_no = seq_no;
    ptr->checksum = checksum;
    ptr->msg_type = msg_type;
}

/*          checksum computation        */
__uint16_t compute_checksum(char *buf, int buf_len)
{
    __uint16_t padd=0;
    __uint16_t word16, received_checksum;
    __uint32_t sum;    

   struct message_t *hdr ;
    hdr = (struct message_t *) buf ;
    //save and make this zero
    received_checksum = hdr->checksum ;
    hdr->checksum  = 0 ; 
   // printf("buf length = %d, received_checksum = 0x%x", buf_len, 
   //     received_checksum);
    if ((buf_len % 2) != 0 ){
        padd=1;
        buf[buf_len]=0;
        
     }
    //initialize sum to zero
    sum=0;

    int i ;
    for (i=0;i<buf_len+padd;i=i+2){
        word16 =((buf[i]<<8)&0xFF00)+(buf[i+1]&0xFF);
        sum = sum + (__uint32_t) word16;
    }   
    //keep only the last 16 bits of the 32 bit calculated sum and add the carries
    while (sum>>16)
        sum = (sum & 0xFFFF)+(sum >> 16);
    sum = ~sum;
    hdr->checksum = received_checksum ; //restoring the checksum in the buffer
  //  printf("The checksum is 0x%x and value in hdr 0x%x\n", sum, hdr->checksum);
      return ((__uint16_t ) sum);
}

int validate_checksum (char *buf , int buf_len){

    char *temp ; 
    temp = buf ; 
   temp = temp + sizeof(__uint32_t);
   __uint16_t *received_checksum = (  __uint16_t *)temp ; 
  // printf("Checksum in the Packet is 0x%x \n", *received_checksum) ;
   __uint16_t newchecksum = compute_checksum(buf, buf_len) ; 
 
   if(newchecksum == *received_checksum) 
       return CHECKSUM_VALID ; 
   else 

       return CHECKSUM_INVALID ;
}

/*          Hexdump             */
void hexdump(void *mem, unsigned int len)
{
        unsigned int i, j;
        
        for(i = 0; i < len + ((len % HEXDUMP_COLS) ? 
                (HEXDUMP_COLS - len % HEXDUMP_COLS) : 0); i++)
        {
                /* print offset */
                if(i % HEXDUMP_COLS == 0)
                {
                        printf("0x%06x: ", i);
                }
 
                /* print hex data */
                if(i < len)
                {
                        printf("%02x ", 0xFF & ((char*)mem)[i]);
                }
                else /* end of block, just aligning for ASCII dump */
                {
                        printf("   ");
                }
                
                /* print ASCII dump */
                if(i % HEXDUMP_COLS == (HEXDUMP_COLS - 1))
                {
                        for(j = i - (HEXDUMP_COLS - 1); j <= i; j++)
                        {
                                if(j >= len) 
                                {
                                    /* end of block, not really printing */
                                        putchar(' ');
                                }
                                else if(isprint(((char*)mem)[j])) 
                                {
                                    /* printable char */
                                        putchar(0xFF & ((char*)mem)[j]);        
                                }
                                else /* other char */
                                {
                                        putchar('.');
                                }
                        }
                        putchar('\n');
                }
        }
}
