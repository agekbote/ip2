

INCLUDE=$(PWD)/include/
BIN=$(PWD)/bin
LIBRARIES=pthread
CURRENT_DIR=$(PWD)

SENDER_SOURCE = $(BIN)/sender.c
RECEIVER_SOURCE = $(BIN)/receiver.c 
COMMON_SOURCE = $(BIN)/common.c

all: sender receiver

sender:	$(SENDER_SOURCE)
	gcc $(SENDER_SOURCE) $(COMMON_SOURCE) -o p2mpclient  -I $(INCLUDE) -l$(LIBRARIES)

receiver:	$(RECEIVER_SOURCE)
	gcc $(RECEIVER_SOURCE) $(COMMON_SOURCE) -o p2mpserver -I $(INCLUDE) -l$(LIBRARIES)

clean:
	rm p2mpclient p2mpserver 

