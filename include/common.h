#include <stdio.h> 
#include<stdlib.h> 

/*Message Data Structure */ 

#define ACK 0xAAAA
#define DATA_MSG 0x5555
#define EOF_FILE 0x6666

#define CHECKSUM_VALID 1 
#define CHECKSUM_INVALID 0 
#define START_SEQNO 0x1234

struct message_t {

    __uint32_t sequence_no ;
    union {
    __uint16_t checksum ;
    __uint16_t all_zeroes;
    };
    __uint16_t msg_type;
};

#ifdef DEBUG_PRINT
    #define debug_printf(fmt, ...) printf(fmt, ##__VA_ARGS__)
#else
    #define debug_printf(fmt, ...)
#endif

#ifdef LOG
    #define log(fmt, ...) printf(fmt, ##__VA_ARGS__)
#else
    #define log(fmt, ...)
#endif

extern void dump_msg(struct message_t * ptr);
extern void prepare_msg(struct message_t * ptr, unsigned int seq_no, 
        unsigned short int checksum, unsigned short int msg_type);

#define HEXDUMP_COLS 8 
extern void hexdump(void *mem, unsigned int len);
