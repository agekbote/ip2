struct receiver_info {
    struct addrinfo * servinfo;
    struct addrinfo * p;
    int fd; 
};

struct receiver_info * receivers;

int mss;
char filename[512];
int num_servers;

char * buffer;
#define BUFFER_MULTIPLIER 2
int next_buffer;
int next_seq = 0;
char * send_buffer;

unsigned short int * ack_array;
int num_acks_received = 0;
pthread_t  timer_thread;
int wnd = 0;

__uint32_t packet_msg_type;
